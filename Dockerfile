FROM python:3.7

RUN apt-get update \
    && apt-get install -y openjdk-8-jre-headless git curl \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app

ADD requirements.txt /app

RUN pip3 install -r requirements.txt

ADD . /app

RUN echo "Installing confluent docker utils" \
    && git clone https://github.com/confluentinc/confluent-docker-utils \
    && cd confluent-docker-utils \
    && python setup.py install > /dev/null 2>&1 \
    && cd .. \
    && rm -rf confluent-docker-utils

RUN echo "Downloading Avro tools" \
    && sh download_avro-tools.sh

EXPOSE 80

ENV NAME avro-extended

CMD ["./run_code_checks.sh"]